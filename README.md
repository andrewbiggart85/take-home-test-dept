# Dept | Take home test

This project is built using a combination of NUXTJS and Tailwindcss. The following steps are based off the assumption that you already have node.js installed on your computer. If that's not the case, you can download it [here](https://nodejs.org/en/download/) before proceeding to the next step.

### Build Setup

``` bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

### Project Dependencies 

##### NuxtJS
[https://nuxtjs.org/](https://nuxtjs.org/)

##### Tailwindcss
[https://tailwindcss.com/](https://tailwindcss.com/)

##### AOS - Animate on scroll library
[https://github.com/michalsnik/aos](https://github.com/michalsnik/aos)

##### Tiny Slider 2
[https://github.com/ganlanyuan/tiny-slider](https://github.com/ganlanyuan/tiny-slider)
